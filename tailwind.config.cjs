/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			screens: {
				xsm: '400px'
				// => @media (min-width: 640px) { ... }
			}
		}
	},
	plugins: []
};
